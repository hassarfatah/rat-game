using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
public class Respawn : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    { }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ResetGame()
    {
        SceneManager.LoadScene("main");
        Debug.Log("resetgame");
    }
    public void ExitGame(){
        QuitApplication();
        Debug.Log("quit");
    }
    public void QuitApplication()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }
}
