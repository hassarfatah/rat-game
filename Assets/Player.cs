using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Player : MonoBehaviour {
    enum Floor {
        Street,
        Trains,
        Canal,
    }
    public GameObject panel;
    public Score score_ref;
    private Floor state;
    private int max_health;
    private int current_health;
    private Animator animator;
    private int cheese_points = 30;
    bool invisible = false;
    void Start() {
        Score.score = 0;
        state = Floor.Canal;
        max_health = gameObject.GetComponent<HealthBar>().numOfHearts;
        current_health = max_health;
    }

    void Update() {
        KeyCode pressed = KeyCode.Space;
        if (Input.GetKeyDown(KeyCode.UpArrow)) {
            pressed = KeyCode.UpArrow;
        } else if (Input.GetKeyDown(KeyCode.DownArrow)){
            pressed = KeyCode.DownArrow;
        }

        nextFloor(pressed);
        transform.position = getFloorPos();
    }
    void OnTriggerEnter2D(Collider2D collision){
        animator = GetComponent<Animator>();
        if (collision.gameObject.tag == "Item_Heal") {
            animator.SetTrigger("player_item_pickup");
            Debug.Log("item_collision");
            if(current_health != max_health){
                current_health++;
                gameObject.GetComponent<HealthBar>().update(current_health);
            }
            else{
                Score.score += 10;
            }
            Destroy(collision.gameObject,0.2f);
        }
        if(collision.gameObject.tag == "item_cheese"){
            animator.SetTrigger("player_item_pickup");
            Score.score += cheese_points;
            Debug.Log("item_collision_cheese");
            Destroy(collision.gameObject,0.2f);
        }
        if(collision.gameObject.tag == "item_invisible"){
            animator.SetTrigger("player_item_invisible");
            invisible = true;
            Invoke("invisible_timer", 3f);
            Debug.Log("item_collision_invisible");
            Destroy(collision.gameObject,0.2f);

        }

        if (collision.gameObject.tag == "Enemy" && invisible == false) {
            Debug.Log("collision");
            current_health--;
            gameObject.GetComponent<HealthBar>().update(current_health);

            if (current_health > 0) {
              animator.SetTrigger("player_damage");
              collision.gameObject.GetComponent<Animator>().SetTrigger("collision_animation");

              panel.SetActive(true);
              Invoke("waitForSeconds", 0.5f); //Calls waitForSeconds() with a delay of 1f
              Destroy(collision.gameObject,0.2f);
            } else {
                animator.SetBool("player_death", true);
                collision.gameObject.GetComponent<Animator>().SetTrigger("collision_animation");

                panel.SetActive(true);
                Invoke("delayGameOver", 0.85f);
                Destroy(gameObject, 0.85f);
            }
        }

    }
    void invisible_timer(){
        animator.SetTrigger("player_item_invisible_end");
        invisible = false;
    }
    void delayGameOver(){
        SceneManager.LoadScene("GameOver");
    }
    void waitForSeconds(){
        panel.SetActive(false);
    }
    void nextFloor(KeyCode direction) {
        switch (state) {
            case Floor.Canal: {
                if (direction == KeyCode.UpArrow) {
                    state = Floor.Trains;
                }
                break;
            }
            case Floor.Street: {
                if (direction == KeyCode.DownArrow) {
                    state = Floor.Trains;
                } 
                break;
            }
            case Floor.Trains: {
                if (direction == KeyCode.UpArrow) {
                    state = Floor.Street;
                } else if (direction == KeyCode.DownArrow){
                    state = Floor.Canal;
                }
                break;
            }
        }
    }
    Vector3 getFloorPos() {
        Vector3 result = new Vector3();
        result.x = (float)-7;
        result.z = (float)0;

        switch (state) {
            case Floor.Canal: {
                result.y = (float)-3.74;
                break;
            }
            case Floor.Trains: {
                result.y = (float)-1.06;
                break;
            }
            case Floor.Street: {
                result.y = (float)1.85;
                break;
            }
        }

        return result;
    }
}
