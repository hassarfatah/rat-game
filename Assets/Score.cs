using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
public class Score : MonoBehaviour {
    private TextMeshProUGUI tmpro;
    public static float score = 0;

    // Start is called before the first frame update
    void Start() {
        tmpro = GetComponent<TextMeshProUGUI>();
        tmpro.text = ((int)score).ToString();
    }

    // Update is called once per frame
    void Update() {
        if(SceneManager.GetActiveScene().name != "GameOver"){
            score += Time.deltaTime;
            tmpro.text = ((int)score).ToString();
        }
    }
}
