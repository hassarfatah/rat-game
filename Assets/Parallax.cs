using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{   
    private float length, startpos;
    public float parallaxEffect;

    // Start is called before the first frame update
    void Start()
    {
        startpos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void Update()
    {
        // Use Time.time for a continuous time-dependent movement
        //float temp = Mathf.Repeat(-1 * Time.time * parallaxEffect, length);
        float dist = Mathf.Repeat(-1 * Time.time * parallaxEffect, length);
        transform.position = new Vector3(startpos + dist, transform.position.y, transform.position.z);
    }
}
