using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class Enemy: MonoBehaviour {
    public float speed = 4f;

    // Start is called before the first frame update
    void Start() {
      TextMeshProUGUI score_component = GameObject.FindGameObjectWithTag("Score").GetComponent<TextMeshProUGUI>();

      int score = int.Parse(score_component.text);
      if (score > 100) {
        speed += 3;
      } else if (score > 50) {
        speed += 2;
      } else if (score > 20) {
        speed += 1;
      }
    }

    // Update is called once per frame
    void Update() {
        transform.position += Vector3.left * Time.deltaTime * speed;

        if (transform.position.x < -13) {
            Destroy(gameObject);
        }
    }
}
