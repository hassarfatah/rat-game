using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    public GameObject enemyPrefab;
    public float spawnRate = 1f;
    private float coolDown;

    // Start is called before the first frame update
    void Start() {
        coolDown = spawnRate;
    }

    // Update is called once per frame
    void Update() {
        coolDown -= Time.deltaTime;
        if (coolDown <= 0 && noCollidingEnemies()){
            spawn();
            coolDown = spawnRate * Random.Range((float)1.2,4);
        }
    }
    void spawn() {
        Instantiate<GameObject>(enemyPrefab,transform.position,Quaternion.identity);
    }
    // checks that when an enemy spawns that jump is doable in that range
    bool noCollidingEnemies() {
      GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

      foreach (GameObject enemy in enemies){
        if (enemy.transform.position.x > 7.5) {
          return false;
        }
      } 

      return true;
    }
}
