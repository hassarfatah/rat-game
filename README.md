## RAT-GAME

### Spielanleitung:
Ebenen wechseln über Pfeiltasten (hoch/runter), um Gegnern auszuweichen.
Man verliert Herzen wenn man einen Gegner trifft.
Wenn man alle Herzen verliert ist das Spiel vorbei.

### Items:
Käse einsammeln: + 30 Punkte
Herz einsammeln: Wenn volles Leben: + 10 Punkte
                 Wenn nicht: extra Leben
Pille: Unsichtbar

Sidenote: Wir haben alle auf einem Mac entwickelt, aber in den Requirements steht, dass das Spiel
für entweder Windows oder Linux laufen soll. Da man in den in den Build-Settings nicht Cross-Compilen kann,
mussten wir das Spiel für MacOs bauen. Wenn Sie das nicht überprüfen können, wäre es nett wenn Sie uns schreiben könnten,
damit wir gucken ob wir es noch für andere Platformen builden können.
